<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tenistas".
 *
 * @property int $id
 * @property string $nombre
 * @property int $altura
 * @property int $peso
 * @property string $correo
 * @property string $fechanacimiento
 * @property string $fechabaja
 * @property int $activo
 * @property int $nacion
 *
 * @property Naciones $nacion0
 */
class Tenistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tenistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['altura', 'peso', 'activo', 'nacion'], 'integer'],
            [['fechanacimiento', 'fechabaja'], 'safe'],
            [['nombre'], 'string', 'max' => 20],
			[['correo'], 'email'],
            [['nacion'], 'exist', 'skipOnError' => true, 'targetClass' => Naciones::className(), 'targetAttribute' => ['nacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'altura' => 'Altura (cm)',
            'peso' => 'Peso (kg)',
            'correo' => 'Correo',
            'fechanacimiento' => 'Fecha Nacimiento',
            'fechabaja' => 'Fecha Baja',
            'activo' => 'Activo',
            'nacion' => 'Nación',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNacion0()
    {
        return $this->hasOne(Naciones::className(), ['id' => 'nacion']);
    }
}

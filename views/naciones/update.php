<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Naciones */

$this->title = 'Actualizar Naciones: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Naciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="naciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
